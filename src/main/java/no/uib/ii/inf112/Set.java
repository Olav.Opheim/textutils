package no.uib.ii.inf112;

import java.util.Collection;

public interface Set<T> extends Iterable<T> {

	boolean contains(T obj);
	
	void add(T obj);
	

	/**
	 * @param obj
	 * @throws IllegalArgumentException if not this.contains(obj)
	 */
	void remove(T obj);
	
	/**
	 * @return Size of set >= 0
	 */
	int size();
	
	String toString();
	
	boolean equals(Object other);
	
	int hashCode();
}
